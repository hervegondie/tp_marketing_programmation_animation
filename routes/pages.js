const express = require('express'),
      router = express.Router(),
      pageControllers = require('../controllers/pageControllers');
      

     
      router.get('/', pageControllers.getPages)
      router.get('/:id', pageControllers.getPageById); 

    
      module.exports = router;