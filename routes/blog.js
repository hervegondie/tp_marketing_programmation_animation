const express = require('express'),
      router = express.Router(),
      blogController = require('../controllers/blogController');
      

     
      router.get('/blog', blogController.getBlog)
      router.get('/:id', blogController.getBlogById); 

      module.exports = router;