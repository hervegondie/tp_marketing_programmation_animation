const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

let BlogSchema = new Schema({
    _id : Mongoose.Schema.Types.ObjectId,
    titre: String,
    categorie: String,
    auteur: String,
    contenu: String,
    date: Number
}, {
    timestamps: true
});

const blogModel = Mongoose.model("Blog", BlogSchema)

blogModel.find()
    .exec()
    .then(blog=>{
        console.log(blog)
    })
    .catch()

module.exports = {
    model:blogModel,
    getBlog :  async () => {
        return await blogModel.find({});
    },
    getBlogById : async (id) => {
        return await blogModel.findById(id);
        
    } 
}