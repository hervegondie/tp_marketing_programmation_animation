const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

let PageSchema = new Schema({
    _id : Mongoose.Schema.Types.ObjectId,
    titre: String,
    contenu: String
}, {
    timestamps: true
});

const pagePrincipalModel = Mongoose.model("Page", PageSchema)

pagePrincipalModel.find()
    .exec()
    .then(pages=>{
        console.log(pages)
    })
    .catch()

// let BlogSchema = new Schema({
//     _id : Mongoose.Schema.Types.ObjectId,
//     titre: String,
//     categorie: String,
//     auteur: String,
//     Contenu: String,
//     date: Number
// }, {
//     timestamps: true
// });

// const blogModel = Mongoose.model("Blog", BlogSchema)

// blogModel.find()
//     .exec()
//     .then(blog=>{
//         console.log(blog)
//     })
//     .catch()

module.exports = {
    model:pagePrincipalModel,
    getPages:  async () => {
        return await pagePrincipalModel.find({})
    },
    getPageById: async (id) => {
        return await pagePrincipalModel.findById(id);
    } 

    // model:blogModel,
    // getBlog:  async () => {
    //     return await blogModel.find({})
    // },
    // getBlogById: async (id) => {
    //     return await blogModel.findById(id);

    // },
}

// module.exports = {
//     model:blogModel,
//     getBlog:  async () => {
//         return await blogModel.find({})
//     },
//     getBlogById: async (id) => {
//         return await blogModel.findById(id);

//     }, 
// }