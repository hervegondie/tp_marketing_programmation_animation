const blogModel = require("../modeles/blogModel");

module.exports = {
    getBlog: async (req,res,next) => {
        try{
            let documents = await blogModel.getBlog();
            res.render('blog.hbs', { blog: documents});
        } catch ( err ){
            console.log(err);
            res.status(500).send("erreur interne");
        }
    },
    getBlogById: async (req,res,next) =>{
        try {
            let document = await blogModel.getBlogById(req.params.id);
            res.render({document})
        } catch (error) {
            console.log(err);
            res.status(500).send("erreur interne");
        }
    }

}