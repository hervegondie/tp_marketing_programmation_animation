const pagePrincipalModel = require("../modeles/pagePrincipalModel");

module.exports = {
    getPages: async (req,res,next) => {
        try{
            let documents = await pagePrincipalModel.getPages();
            res.render('pages.hbs', { pages: documents});
        } catch ( err ){
            console.log(err);
            res.status(500).send("erreur interne");
        }
    },
    getPageById: async (req,res,next) =>{
        try {
            let document = await pagePrincipalModel.getPageById(req.params.id);
            res.render({document})
        } catch (err) {
            console.log(err);
            res.status(500).send("erreur interne");
        }
    }

    // getBlog: async (req,res,next) => {
    //     try{
    //         let documents = await pagePrincipalModel.getBlog();
    //         res.render('blog.hbs', { blog: documents});
    //     } catch ( err ){
    //         console.log(err);
    //         res.status(500).send("erreur interne");
    //     }
    // },
    // getBlogById: async (req,res,next) =>{
    //     try {
    //         let documents = await blogModel.getBlogById(req.params.id);
    //         res.render({documents})
    //     } catch (err) {
    //         console.log(err);
    //         res.status(500).send("erreur interne");
    //     }
    // }
};